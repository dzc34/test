# Prerequisites for web application installation

## Prerequesites

- PHP >= **7.3.5**
- composer
- PHP extensions:
    - `ext-ctype`  `*`
    - `ext-curl`
    - `ext-iconv`  `*`
    - `ext-json`
    - `ext-PDO`   `*`
    - `ext-tokenizer`   `*`
    - `ext-xml`
    - `ext-zip`

`*`  installed by default with PHP via the `php-common` package (Ubuntu 18.04)

## Ubuntu 18.04

### 0. Verify prerequisites

#### PHP version

```bash
# List php versions available on the host:
sudo update-alternatives --list php

# Display the default php version:
sudo update-alternatives --display php
php -v

# If suitable PHP version is available, enable it:
sudo update-alternatives --set php /usr/bin/php7.3
php -v
```

#### Checks webapp prerequisites

Checks that PHP and extensions versions match the platform requirements of the installed packages:

```bash
git clone <projectUrl>/<repositoryName>
cd <repositoryName>/webapp/
composer check-platform-reqs
```

Documentation: https://getcomposer.org/doc/03-cli.md#check-platform-reqs


### 1. Install correct PHP version + extensions

```bash
sudo apt-get update
sudo apt install    \
    php7.3          \
    php7.3-curl     \
    php7.3-xml      \
    php7.3-mbstring     \
        # php7.3-json     \
        # php7.3-zip
```
ext-curl ---> composer
ext-xml  ---> ??? ---> composer.json
ext-mbstring ---> phpunit

### 2. Install composer

```bash
sudo apt-get update
sudo apt install   composer
```

### 3. Enabled the PHP version needed by webapp

```bash
sudo update-alternatives --set php /usr/bin/php7.3
php -v
```

and verify again with:

```bash
composer check-platform-reqs
```

### Optional


#### Download `composer` packages in parallel
- [hirak/prestissimo](https://packagist.org/packages/hirak/prestissimo), a composer plugin that downloads packages in parallel
- `hirak/prestissimo` must be installed globaled for the user who launch `composer install` command.

```bash
composer global require hirak/prestissimo

    # hirak/prestissimo requires ext-curl
    php -v
    sudo apt -y install php<Major.Minor>-curl
```


#### Symfony CLI tool (dev|test environnemnets)

- not install and **not use** it for **production**
- only for dev and test environnemnets
    - start a web server (HTTP or HTTP**S**)
    - display server logs and symfony framework logs

To install it you have to follow the [procedure](https://symfony.com/download) in the Symfony documentation.
and consult our [memento](../90_Mementos/memento_Symfony-CLI.md).
