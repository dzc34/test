# Web application installation

You should have already [check pre-requisites](webapp_Pre-requisites.md).

## Ubuntu 18.04

### Grab source code
```bash
# Grab source code and installing dependencies
git clone <projectUrl>/<repositoryName>
cd <repositoryName>/webapp/
composer install
```

### Starting a local web server (not use it for production)

- **not use** it for **production**
- only for dev and test environnemnets

```bash
# via PHP (not use it for production)
cd webapp/
php -S 127.0.0.1:8383 -t public/

# via Symfony CLI (not use it for production)
cd webapp/
symfony serve
```

We recommend you to use the **Symfony CLI** option for a development environment
and you can consult our [memento](../90_Mementos/memento_Symfony-CLI.md) to understand how to use it.
