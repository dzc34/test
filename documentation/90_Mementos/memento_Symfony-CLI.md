# Symfony CLI tool (dev|test environnemnets)

- not install and **not use** it for **production**
- only for dev and test environnemnets
    - start a web server (HTTP or HTTP**S**)
    - display server logs and symfony framework logs

To install it you have to follow the [procedure](https://symfony.com/download) in the Symfony documentation.

## HTTPS

If you want to use the HTTPS protocol, you need to install the `libnss3-tools` debian package.
```bash
sudo apt install libnss3-tools
symfony  server:ca:install     # Create a local Certificate Authority for serving HTTPS
```

## Usages (cheatsheet)

```bash
# List available PHP versions
symfony  local:php:list        # List locally available PHP versions
symfony  local:php:refresh     # Auto-discover the list of available PHP version

# Create a local Certificate for serving HTTPS
symfony  server:ca:install     # Create a local Certificate Authority for serving HTTPS
symfony  server:ca:uninstall   # Uninstall the local Certificate Authority

# Run a local web server
symfony  serve                 # Run a local web server
symfony  serve  -d             # Run a local web server in the background
symfony  server:log            # Display local web server logs
symfony  server:status         # Get the local web server status
symfony  server:stop           # Stop the local web server
symfony  server:list           # List all running local web servers (launched with command "symfony serve"
symfony  server:prod           # Switch a project to use Symfony's production environment
symfony  server:prod --off     # Disable prod mode

# Open in a browser (local website, symfony doc, ...)
symfony  open:docs             # Open the online Web documentation
symfony  open:local            # Open the local project in a browser
symfony  open:local:rabbitmq   # Open the local project RabbitMQ web management interface in a browser
symfony  open:local:webmail    # Open the local project mail catcher web interface in a browser

# Book "Symfony 5: The Fast Track"
# see: https://github.com/the-fast-track/book-5.0-1
symfony  book:check            # Check that you have all the pre-requisites locally to code while reading the book
symfony  book:checkout         # Check out a step of the "Symfony 5: The Fast Track" book repository
```
