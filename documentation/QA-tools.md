# QA

## URL
- [http](http://soutenir.librairielacavale.coop)
- [https](https://soutenir.librairielacavale.coop) :
- Files:
    * [/humans.txt](https://soutenir.librairielacavale.coop/humans.txt)
    * [/robots.txt](https://soutenir.librairielacavale.coop/robots.txt)
    * [/.well-known/security.txt](https://soutenir.librairielacavale.coop/.well-known/security.txt)

````
https://soutenir.librairielacavale.coop
http://soutenir.librairielacavale.coop
https://soutenir.librairielacavale.coop/humans.txt
https://soutenir.librairielacavale.coop/robots.txt
https://soutenir.librairielacavale.coop/.well-known/security.txt
````


## QA - Online tools
`*` preconfigured tools

* HTTP Response :
    * [httpstatus.io](https://httpstatus.io/)
    * [URLitor - HTTP Status & Redirect Checker](http://www.urlitor.com/)
    * [HTTP Response Checker](https://www.webmoves.net/tools/responsechecker)
    * [Server Headers](http://tools.seobook.com/server-header-checker/?url=https%3A%2F%2Fsoutenir.librairielacavale.coop%0D%0Ahttps%3A%2F%2Fsoutenir.librairielacavale.coop%0D%0Ahttp%3A%2F%2Fsoutenir.librairielacavale.coop%0D%0Ahttp%3A%2F%2Fsoutenir.librairielacavale.coop%0D%0A&useragent=11&protocol=11) `*`
* Security
    * [Hardenize](https://www.hardenize.com) (DNS, SMTP, web server)
    * [Mozilla Observatory](https://observatory.mozilla.org/analyze/soutenir.librairielacavale.coop) `*` (HTTP header, SSL, cookies, ...)
    * [Security Headers](https://securityheaders.io/?q=https://soutenir.librairielacavale.coop) `*` (HTTP header)
    * Content-Security-Policy (CSP)
        * [cspvalidator.org](https://cspvalidator.org/#url=https://soutenir.librairielacavale.coop) `*`
        * [csp-evaluator.withgoogle.com](https://csp-evaluator.withgoogle.com/?csp=https://soutenir.librairielacavale.coop) `*`
    * SSL
        * [ssllabs.com](https://www.ssllabs.com/ssltest/analyze?d=soutenir.librairielacavale.coop) `*`
        * [tls.imirhil.fr](https://tls.imirhil.fr/https/soutenir.librairielacavale.coop) `*`
    * DNS
        * [DNSViz](http://dnsviz.net/d/soutenir.librairielacavale.coop/dnssec/) `*` (DNSSEC)
        * [DNSSEC Analyzer (Verisign Labs)](https://dnssec-debugger.verisignlabs.com/soutenir.librairielacavale.coop) `*`   (DNSSEC)
        * [Zonemaster (iiS and AFNIC)](https://zonemaster.net/domain_check)
* W3C tools
    * [HTML validator](https://validator.w3.org/nu/?doc=https://soutenir.librairielacavale.coop&showsource=yes&showoutline=yes&showimagereport=yes) `*`
    * [CSS validator](https://jigsaw.w3.org/css-validator/validator?uri=https://soutenir.librairielacavale.coop&profile=css3) `*`
    * [i18n checker](https://validator.w3.org/i18n-checker/check?uri=https://soutenir.librairielacavale.coop) `*`
    * [Link checker](https://validator.w3.org/checklink?uri=https://soutenir.librairielacavale.coop&hide_type=all&depth=&check=Check) `*`
* Web accessibility
    * [Asqatasun](https://app.asqatasun.org)
* Web perf
    * [Yellowlab](http://yellowlab.tools)
    * [Webpagetest](https://www.webpagetest.org/)
    * [Test a single asset from 14 locations](https://tools.keycdn.com/performance?url=https://soutenir.librairielacavale.coop) `*`
* HTTP/2
    * [Http2.pro](https://http2.pro/check?url=https://soutenir.librairielacavale.coop) `*` (check server HTTP/2, ALPN, and Server-push support)
* Global tools (webperf, accessibility, security, ...)
    * [Dareboost](https://www.dareboost.com)  (free trial)
    * [Sonarwhal](https://sonarwhal.com/scanner/)


------

* Social networks
  * [Twitter card validator](https://cards-dev.twitter.com/validator)
* structured data (JSON-LD, rdf, schema.org, microformats.org, ...)
  * [Google structured data testing tool](https://search.google.com/structured-data/testing-tool#url=https://soutenir.librairielacavale.coop/)  `*`
  * [Structured Data Linter](http://linter.structured-data.org/?url=https://soutenir.librairielacavale.coop)  `*`
  * [Microdata Parser](https://www.webmoves.net/tools/microdata)
* Google image
  * [images used on the website](https://www.google.fr/search?tbm=isch&q=site:soutenir.librairielacavale.coop)  `*`  (site:soutenir.librairielacavale.coop)
  * [images used on the website but hosted on other domains](https://www.google.fr/search?tbm=isch&q=site:soutenir.librairielacavale.coop+-src:soutenir.librairielacavale.coop) `*`  (site:soutenir.librairielacavale.coop -src:soutenir.librairielacavale.coop)
  * [images hosted on the domain name](https://www.google.fr/search?tbm=isch&q=src:soutenir.librairielacavale.coop)  `*`    (src:soutenir.librairielacavale.coop)
  * [images hosted on the domain name and used by other domain names (hotlinks)](https://www.google.fr/search?tbm=isch&q=src:soutenir.librairielacavale.coop+-site:soutenir.librairielacavale.coop)  `*`   (src:soutenir.librairielacavale.coop -site:soutenir.librairielacavale.coop)


## QA - Open-source softwares

* [W3C tools](https://w3c.github.io/developers/tools/#tools)
* Security
    * [Arachni](https://github.com/Arachni/arachni) (web application security scanner framework)
    * Content-Security-Policy (CSP)
        * [salvation](https://github.com/shapesecurity/salvation) (Java parser, warn about policy errors)
    * Mozilla Observatory
        * [CLI client for Mozilla Observatory](https://github.com/mozilla/observatory-cli)
        * [HTTP Observatory](https://github.com/mozilla/http-observatory) (local scanner : CLI and CI)
* Web accessibility
    * Asqatasun
        * [Asqatsun Docker image](https://hub.docker.com/r/asqatasun/asqatasun/)
        * [Install Asqatasun on a server](https://doc.asqatasun.org/en/10_Install_doc/Asqatasun/)
* Web perf
    * Webpagetest
    * [Yellowlab](https://github.com/gmetais/YellowLabTools/) (API, npm CLI, Grunt task, ...)
    * [Sitespeed.io](https://www.sitespeed.io/) (npm or docker is needed)
* Global tools
    * [Sonarwhal](https://github.com/sonarwhal/sonarwhal) (Node.js v8)
