# Developer documentation

Here you will find information for people willing
to understand the internals of **Bookstore support**
or bring help to its development.

# Getting started
- [Versioning, Changelog and GIT Commit message](GIT_CONVENTION.md)
- [Install documentation](../10_Install_doc/)

# Start coding

```bash
# Grab source code and installing dependencies
git clone <projectUrl>/<repositoryName>
cd <repositoryName>/webapp/
composer install

# Starting a local web server

    # via PHP (not use it for production)
    cd webapp/
    php -S 127.0.0.1:8383 -t public/

    # via Symfony CLI (not use it for production)
    cd webapp/
    symfony serve
```

We recommend you to use the **Symfony CLI** option for a development environment
and you can consult our [memento](../90_Mementos/memento_Symfony-CLI.md) to understand how to use it.



## Units tests and Functional tests
- https://symfony.com/doc/current/testing.html
- https://symfony.com/doc/current/testing/database.html
- https://symfony.com/doc/current/testing/functional_tests_assertions.html

```bash
cd webapp/

# Run tests
bin/phpunit
bin/phpunit  --group ...         # Only runs tests from the specified group(s)
bin/phpunit  --exclude-group ... # Exclude tests from the specified group(s)
bin/phpunit  --testdox-text tests/doc_testsList.txt     # run and generated doc
bin/phpunit  --testdox-html tests/doc_testsList.html    # run and generated doc

# Add functionnal tests and unit tests
bin/console make:functional-test  # Creates a new functionnal test
bin/console make:unit-test        # Creates a new unit test

bin/phpunit   help
bin/phpunit  --list-groups       # List available test groups
bin/phpunit  --list-suites       # List available test suites
bin/phpunit  --list-tests        # List available tests
```

## Symfony Console

```bash
cd webapp/
bin/console about            # Displays information about the current project
bin/console help  <command>  # Displays help for a command
bin/console list             # Lists commands

bin/console doctrine:database:create    # Creates  a new database
bin/console make:migration              # Creates  a new database migration based on entities changes
bin/console doctrine:migration:migrate  # Executes a migration to a specified version or the latest available version.
bin/console make:fixtures               # Creates a new class to load Doctrine fixtures
bin/console doctrine:fixtures:load      # Load data fixtures to your database
...
bin/console make:controller # Creates a new controller class
bin/console make:crud       # Creates CRUD for Doctrine entity class
bin/console make:entity     # Creates or updates a Doctrine entity class, and optionally an API Platform resource
bin/console make:form       # Creates a new form class
...
bin/console debug:router             # Lists all the configured routes in your application
bin/console debug:router <routeName> # Get very specific information on a single route
bin/console router:match <routePath> # Find out which route is associated with the given URL (ex: /blog/)

```



