[![License : AGPL v3](https://img.shields.io/badge/license-AGPL3-blue.svg)](LICENSE)
[![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](CONTRIBUTING.md)
[![Code of Conduct](https://img.shields.io/badge/code%20of-conduct-ff69b4.svg?style=flat-square)](CODE_OF_CONDUCT.md)

# Bookstore support

## Documentation

- [QA : URLs + online tools](documentation/QA-tools.md)
- [Install documentation](documentation/10_Install_doc/)
- [Developer documentation](documentation/30_Contributor_doc/)
- [Mementos](documentation/90_Mementos/)

## License

[AGPL v3](LICENSE)
